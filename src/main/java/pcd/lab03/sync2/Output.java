package pcd.lab03.sync2;

import java.util.concurrent.Semaphore;

public class Output {
	
	private Semaphore mutex;
	
	public Output(){
		mutex = new Semaphore(1);
	}
	
	public void print(String msg){
		try {
			mutex.acquire();
			System.out.println(msg);
		} catch (Exception ex){
			ex.printStackTrace();
		} finally {
			mutex.release();
		}
	}

}
