package pcd.lab12.rest.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.HashMap;
import java.util.Map;

public class MyService extends AbstractVerticle {

	private Map<String, JsonObject> resources = new HashMap<>();
	private int count;

	@Override
	public void start() {

		setup();

		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());
		router.get("/api/resources/:resID").handler(this::handleGetRes);
		router.post("/api/resources").handler(this::handleAddRes);
		router.put("/api/resources/:resID").handler(this::handleUpdateRes);
		router.get("/api/resources").handler(this::handleListRes);

		vertx.createHttpServer().requestHandler(router::accept).websocketHandler(this::webSocketHandler).listen(8080);
		System.out.println("Service ready.");
	}

	private void handleGetRes(RoutingContext routingContext) {
		String resID = routingContext.request().getParam("resID");
		HttpServerResponse response = routingContext.response();
		if (resID == null) {
			sendError(400, response);
		} else {
			JsonObject res = resources.get(resID);
			if (res == null) {
				sendError(404, response);
			} else {
				response.putHeader("content-type", "application/json").end(res.encodePrettily());
			}
		}
	}

	private void handleAddRes(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		JsonObject res = routingContext.getBodyAsJson();
		if (res == null) {
			sendError(400, response);
		} else {
			String resID = res.getString("id");
			resources.put(resID, res);
			response.putHeader("content-type", "application/json").setStatusCode(201).end(res.encodePrettily());
		}
	}

	private void handleUpdateRes(RoutingContext routingContext) {
		String resID = routingContext.request().getParam("resID");
		HttpServerResponse response = routingContext.response();
		if (resID == null) {
			sendError(400, response);
		} else {
			JsonObject res = routingContext.getBodyAsJson();
			if (res == null) {
				sendError(400, response);
			} else {
				resources.put(resID, res);
				response.putHeader("content-type", "application/json").end(res.encodePrettily());
			}
		}
	}

	private void handleListRes(RoutingContext routingContext) {
		JsonArray arr = new JsonArray();
		resources.forEach((k, v) -> arr.add(v));
		routingContext.response().putHeader("content-type", "application/json").end(arr.encodePrettily());
	}

	private void webSocketHandler(io.vertx.core.http.ServerWebSocket ws) {
		System.out.println("WebSocket opened.");
		ws.handler(hnd -> {
			JsonObject obj = hnd.toJsonObject();
			System.out.println("data received: " + obj.toString());
			count++;
			obj.put("count", count);
			Buffer buffer = Buffer.buffer();
			obj.writeToBuffer(buffer);
			ws.write(buffer);
		});
		// if (req.uri().equals("/")) req.response().sendFile(path+"/ws.html");
	}

	private void setup() {

		// initial data
		addRes(new JsonObject().put("id", "res1").put("prop", 10));
		addRes(new JsonObject().put("id", "res2").put("prop", 20));
		addRes(new JsonObject().put("id", "res3").put("prop", 30));

		count = 0;
	}

	private void addRes(JsonObject res) {
		resources.put(res.getString("id"), res);
	}

	private void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		MyService service = new MyService();
		vertx.deployVerticle(service);
	}

}