package pcd.lab04.monitors.barrier.sol;

public interface Barrier {

	void hitAndWaitAll() throws InterruptedException;

}
