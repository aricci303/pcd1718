package pcd.lab04.monitors.resman;

/*
 * Barrier - to be implemented
 */
public class FakeResManager implements ResManager {

	private int nResourcesAvailable;
	private int nResourcesUsed;
	private boolean[] resFreeMap;
	
	public FakeResManager(int nResourcesAvailable) {
		this.nResourcesAvailable = nResourcesAvailable;
		nResourcesUsed = 0;
		resFreeMap = new boolean[nResourcesAvailable];
		for (int i = 0; i< nResourcesAvailable; i++) {
			resFreeMap[i] = true;
		}
	}
	
	@Override
	public synchronized int get() throws InterruptedException {
		while (nResourcesUsed == nResourcesAvailable ) {
			wait();
		}
		int id = getFreeResource();
		resFreeMap[id] = false;
		nResourcesUsed++;
		return id;
	}

	@Override
	public synchronized void release(int id) {
		resFreeMap[id] = true;
		nResourcesUsed--;
		if (nResourcesUsed == nResourcesAvailable - 1) {
			notify();
		}
	}
	
	private int getFreeResource() {
		for (int i = 0; i< nResourcesAvailable; i++) {
			if (resFreeMap[i]) {
				return i;
			}
		}
		return -1;
	}

	
}
