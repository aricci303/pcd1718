package pcd.lab07.rx;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.schedulers.Schedulers;

public class Test5hot {

	public static void main(String[] args) throws Exception {

		Flowable<Integer> source = Flowable.create(emitter -> {		     
			new Thread(() -> {
				int i = 0;
				while (i < 100){
					try {
						System.out.println(Thread.currentThread()+" GEN "+i); 
						emitter.onNext(i);
						Thread.sleep(1000);
						i++;
					} catch (Exception ex){}
				}
			}).start();
		     //emitter.setCancellable(c::close);
		 }, BackpressureStrategy.BUFFER);

		ConnectableFlowable<Integer> hotObservable = source.publish();
		hotObservable.connect();
	
		Thread.sleep(5000);
		System.out.println("Subscribing.");
		
		hotObservable.subscribe((s) -> {
			System.out.println(Thread.currentThread()+" A "+s); 
		});	
		
		hotObservable.subscribe((s) -> {
			System.out.println(Thread.currentThread()+" B "+s); 
			// while (true) {}
		});	
		
		System.out.println("Done.");

	}

}
