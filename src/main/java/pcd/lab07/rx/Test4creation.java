package pcd.lab07.rx;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

public class Test4creation {

	public static void main(String[] args) throws Exception {

		System.out.println("Creating the source.");

		Flowable<Integer> source = Flowable.create(emitter -> {		     
			new Thread(() -> {
				int i = 0;
				while (i < 100){
					try {
						System.out.println("generator "+Thread.currentThread()+" => "+i); 
						emitter.onNext(i);
						Thread.sleep(1000);
						i++;
					} catch (Exception ex){}
				}
			}).start();
		     //emitter.setCancellable(c::close);
		 }, BackpressureStrategy.BUFFER);

		// Thread.sleep(5000);
		System.out.println("Subscribing.");
		
		source.subscribe((s) -> {
			System.out.println(Thread.currentThread()+" A=> "+s); 
			while (true){}
		});	

		source.subscribe((s) -> {
			System.out.println(Thread.currentThread()+" B=>  "+s); 
			// while (true){}
		});	
		
		System.out.println("Done.");
	}

}
