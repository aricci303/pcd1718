package pcd.lab07.vertx.futures;

import io.vertx.core.AsyncResult;
import io.vertx.core.*;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;

public class TestFuture2 {

	public static void main(String[] args) {
		Vertx  vertx = Vertx.vertx();
		FileSystem fs = vertx.fileSystem();    		
		Future<Buffer> f1 = Future.future();
		
		fs.readFile("build.gradle", f1.completer());
		
		f1.setHandler((AsyncResult<Buffer> res) -> {
			System.out.println("res => "+res);
		});
		// System.out.println("HERE");
	}

}

