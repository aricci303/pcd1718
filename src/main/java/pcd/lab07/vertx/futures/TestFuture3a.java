package pcd.lab07.vertx.futures;

import io.vertx.core.AsyncResult;
import io.vertx.core.*;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;

class MyVerticle extends AbstractVerticle {
	public void start() {
		FileSystem fs = getVertx().fileSystem();    		
		Future<Buffer> f1 = Future.future();
		Future<Buffer> f2 = Future.future();
		fs.readFile("build.gradle", f1.completer());
		fs.readFile("settings.gradle", f2.completer());
		f1.setHandler((AsyncResult<Buffer> res) -> {			
			System.out.println("BUILD => "+Thread.currentThread()+"\n"+res.toString().substring(0,160));
		});
		f2.setHandler((AsyncResult<Buffer> res) -> {
			System.out.println("SETTINGS =>"+Thread.currentThread()+" \n"+res.toString().substring(0,160));
		});
		
	}
}
public class TestFuture3a {

	public static void main(String[] args) {
		Vertx  vertx = Vertx.vertx();
		vertx.deployVerticle(new MyVerticle());
	}

}

