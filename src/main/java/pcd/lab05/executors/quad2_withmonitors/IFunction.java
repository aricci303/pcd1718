package pcd.lab05.executors.quad2_withmonitors;

public interface IFunction {

	public double eval(double val);
	
}
