package pcd.lab05.executors.deadlock;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Test {

	public static void main(String[] args) throws Exception {

		int nThreads = Runtime.getRuntime().availableProcessors() + 1;
		int nTasks = nThreads + 1;
		
		ExecutorService exec = Executors.newFixedThreadPool(nThreads);
		CyclicBarrier barrier = new CyclicBarrier(nTasks);
				
		for (int i = 0; i < nTasks; i++) {
			exec.execute(new MyTask("task-"+i, barrier));
		}		
		
		exec.shutdown();
		exec.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);		
	}
}

class MyTask implements Runnable {
	
	private String name;
	private CyclicBarrier barrier;
	
	public MyTask(String name, CyclicBarrier barrier) {
		this.name = name;
		this.barrier = barrier;
	}
	
	public void run() {
		log("a");
		try {
			barrier.await();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		log("b");
	}
	
	private void log(String msg) {
		synchronized(System.out) {
			System.out.println("["+name+"] "+msg);
		}
	}
}

