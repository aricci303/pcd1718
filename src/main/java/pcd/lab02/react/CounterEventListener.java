package pcd.lab02.react;

public interface CounterEventListener {
	void counterChanged(CounterEvent ev);
}
