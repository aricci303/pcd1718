package pcd.ass02.ex2;

import static pcd.ass02.ex2.Channels.*;

import io.reactivex.processors.FlowableProcessor;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.file.FileSystem;
import pcd.ass02.model.DataPiece;
import pcd.ass02.model.TaskData;
import pcd.ass02.utils.Utils;

;

public class VerticleFileAnalyzer extends AbstractVerticle {
    private TaskData data;
    private FlowableProcessor<DataPiece> emitter;

    public VerticleFileAnalyzer(TaskData data){
        this.data = data;
        this.emitter = emitter;
    }

    @Override public void start(){
        EventBus eb = vertx.eventBus();

        //K i = new K(0);

        eb.consumer(CHANNEL_FILE, fileTask -> {
            String filePath = fileTask.body().toString();
            //i.add(1);
            //System.out.println("Process file #"+i.get()+": " + filePath);
            vertx.fileSystem().readFile(filePath, buffer -> {
                VertxUtils.HandleError(buffer);
                vertx.executeBlocking(future -> {
                    future.complete(Utils.numMatches(data.getRegex(), buffer.toString()));
                }, false, res -> {
                    VertxUtils.HandleError(res);
                    int numOccs = (int)res.result();
                    vertx.eventBus().send(CHANNEL_EVENT, filePath+":"+numOccs);
                    vertx.setTimer(500, delay -> vertx.eventBus().send(CHANNEL_END, -1));
                    //if (numOccs > 0)
                    //    System.out.println(filePath + " >> " + numOccs);
                });
            });
        });
    }

    @Override public void stop(){ System.out.println("Stopped VerticleFileAnalyzer"); }
}
