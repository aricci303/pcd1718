package pcd.ass02.ex0;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import pcd.ass02.model.TaskData;
import pcd.ass02.model.TaskResult;
import pcd.ass02.utils.K;
import pcd.ass02.utils.Utils;

public class FilesystemRegexpSearcher implements Runnable {
    private TaskData data;
    private AtomicBoolean stopCondition;
    private Consumer<TaskResult> consumer;
    private Consumer<TaskResult> onComplete;
    private TaskResult result;

    public FilesystemRegexpSearcher(TaskData data, AtomicBoolean stopCondition, Consumer<TaskResult> consumer, Consumer<TaskResult> onComplete){
        this.data = data;
        this.stopCondition = stopCondition;
        this.consumer = consumer;
        this.onComplete = onComplete;
    }

    @Override
    public void run() {
        result = new TaskResult();
        List<File> files = Utils.getFiles(data.getBasePath(), data.getDepth());
        final K analysedFiles = new K(0);
        final K numMatchedFiles = new K(0);
        final K numMatchings = new K(0);
        final List<String> matchedFiles = new ArrayList<>();

        for(File file : files){
            if(stopCondition.compareAndSet(true,false)) break;
            try {
                String content = new String(Files.readAllBytes(file.toPath()));
                int numMatches = Utils.numMatches(data.getRegex(), content);
                analysedFiles.add(1);
                if(numMatches>0) {
                    numMatchedFiles.add(1);
                    matchedFiles.add(file.getPath());
                }
                numMatchings.add(numMatches);
                result.put(file.getPath(), numMatches);
                consumer.accept(result);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        onComplete.accept(result);
    }
}
