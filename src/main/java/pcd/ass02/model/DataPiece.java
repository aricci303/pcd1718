package pcd.ass02.model;

public class DataPiece {
    private String path;
    private int matches;

    public DataPiece(String path, int matches){ this.path = path; this.matches = matches; }

    public String getPath(){ return this.path; }
    public int getCount(){ return this.matches; }

    @Override
    public String toString() {
        return this.path + " --> " + this.matches;
    }
}
