package pcd.lab08.rxvertx;

import io.vertx.rxjava.core.Vertx;

/*
 * https://vertx.io/docs/vertx-rx/java/
 */
public class SimpleTest {

	public static void main(String[] args) {
		Vertx vertx = io.vertx.rxjava.core.Vertx.vertx();		
		vertx.periodicStream(1000).
			toObservable().
			subscribe(id -> {
	          System.out.println("Callback every second by"+Thread.currentThread());
	        });

		vertx.periodicStream(500).
			toObservable().
			subscribe(id -> {
				System.out.println("Callback every half-second by "+Thread.currentThread());
			});		

		vertx.setPeriodic(250, (res) -> {
			System.out.println("Callback every 250 msecs by "+Thread.currentThread());
		});

		vertx.setPeriodic(2000, (res) -> {
			System.out.println("Callback every 2 secs by "+Thread.currentThread());
			//while (true) {}
		});

	}

}
