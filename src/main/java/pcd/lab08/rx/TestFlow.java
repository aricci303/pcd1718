package pcd.lab08.rx;

import java.util.concurrent.TimeUnit;

import rx.Observable;

public class TestFlow {

	public static void main(String[] args) throws Exception {

		long startTime = System.currentTimeMillis(); 
				
		Observable
        		.interval(100, TimeUnit.MILLISECONDS)
        		.timestamp()
        		.sample(500, TimeUnit.MILLISECONDS)
        		.map(ts -> ts.getTimestampMillis() - startTime + "ms: " + ts.getValue())
        		.take(5)
        		.subscribe(System.out::println);
		
		Thread.sleep(10000);
		
	}

}
