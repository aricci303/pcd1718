package pcd.ass03.chat;

import akka.actor.ActorRef;

public class ParticipantInfo {

	private String nickname;
	private ActorRef who;
	private long lastKeepaliveTimestamp;
	
	public ParticipantInfo(ActorRef who, String nickname){
		this.nickname = nickname;
		this.who = who;
		this.lastKeepaliveTimestamp = System.currentTimeMillis();
	}
	
	public String getNickname(){
		return nickname;
	}
	
	public ActorRef getWho() {
		return who;
	}
	
	public void updateKeepaliveTimestamp(long timestamp) {
		this.lastKeepaliveTimestamp = timestamp;
	}
	
	public long getLastKeepaliveTimestamp() {
		return lastKeepaliveTimestamp;
	}
}
