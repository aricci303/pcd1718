package pcd.ass03.chat.msgs;

import java.io.Serializable;

import akka.actor.ActorRef;

public class TicketRequestMsg implements Serializable {

	private String nickname;
	private ActorRef who;
	
	public TicketRequestMsg(ActorRef who, String nickname){
		this.nickname = nickname;
		this.who = who;
	}
	
	public String getNickname(){
		return nickname;
	}
	
	public ActorRef getWho() {
		return who;
	}
}
