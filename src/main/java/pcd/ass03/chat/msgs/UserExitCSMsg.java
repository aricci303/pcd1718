package pcd.ass03.chat.msgs;

import java.io.Serializable;

import akka.actor.ActorRef;

public class UserExitCSMsg implements Serializable {

	private String nickname;
	
	public UserExitCSMsg(String nickname){
		this.nickname = nickname;
	}
	
	public String getNickname(){
		return nickname;
	}
}
