package pcd.ass03.chat.msgs;

import java.io.Serializable;

import akka.actor.ActorRef;

public class TicketReplyMsg implements Serializable {

	private String nickname;
	private ActorRef who;
	private long ticket;
	
	public TicketReplyMsg(ActorRef who, String nickname, long ticket){
		this.nickname = nickname;
		this.who = who;
	}
	
	public long getTicket() {
		return ticket;
	}
	
	public String getNickname(){
		return nickname;
	}
	
	public ActorRef getWho() {
		return who;
	}
}
