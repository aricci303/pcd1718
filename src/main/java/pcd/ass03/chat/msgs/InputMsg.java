package pcd.ass03.chat.msgs;

public class InputMsg {

	private String msg;
	
	public InputMsg(String msg){
		this.msg = msg;
	}
	
	public String getMsg(){
		return msg;
	}

}
