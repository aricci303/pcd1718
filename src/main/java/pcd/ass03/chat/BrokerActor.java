package pcd.ass03.chat;

import java.time.Duration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import akka.actor.*;
import pcd.ass03.chat.msgs.AddParticipantMsg;
import pcd.ass03.chat.msgs.ChatMsg;
import pcd.ass03.chat.msgs.DispatchChatMsg;
import pcd.ass03.chat.msgs.EnterCSCmdMsg;
import pcd.ass03.chat.msgs.EnterCSMsg;
import pcd.ass03.chat.msgs.ExitCSMsg;
import pcd.ass03.chat.msgs.JoinMsg;
import pcd.ass03.chat.msgs.JoinedMsg;
import pcd.ass03.chat.msgs.NewChatUserMsg;
import pcd.ass03.chat.msgs.NicknameAlreadyUsed;
import pcd.ass03.chat.msgs.QuitMsg;
import pcd.ass03.chat.msgs.RemoveParticipantMsg;
import pcd.ass03.chat.msgs.UserEnteredInCSMsg;
import pcd.ass03.chat.msgs.UserExitCSMsg;
import pcd.ass03.chat.msgs.UserForcedExitCSMsg;

public class BrokerActor extends AbstractActorWithTimers {

	private HashMap<String, ParticipantInfo> participants;

	private boolean isInCriticalSection;
	private Optional<String> actorInCS;

	private final String SOLILOQUIUM_TIMER = "soliloquiumTimer";
	private final long SOLILOQUIUM_TIMEOUT = 10000;
	private final long SOLILOQUIUM_MAX_SILENCE_PERIOD = 8000;
	private long lastMsgTimestamp;

	private AbstractActor.Receive normalModeBehaviour;
	private AbstractActor.Receive soliloquiumBehaviour;

	private LinkedList<String> soliloqPendingRequests;
	
	public void preStart() {
		participants = new HashMap<String, ParticipantInfo>();
		isInCriticalSection = false;
		actorInCS = Optional.empty();
		soliloqPendingRequests = new LinkedList<String>();
		log("ready.");
	}
	
	@Override
	public Receive createReceive() {
		normalModeBehaviour = receiveBuilder().match(AddParticipantMsg.class, this::addParticipant)
		.match(RemoveParticipantMsg.class, this::removeParticipant)
		.match(DispatchChatMsg.class, msg -> {
			lastMsgTimestamp = System.currentTimeMillis();
			log("Chat msg received from " + msg.getSenderNickname() +" at "+lastMsgTimestamp+": "  + msg.getMsg());
			dispatchMsg(new ChatMsg(msg.getSenderNickname(), msg.getMsg()));
		}).match(EnterCSMsg.class, msg -> {
			log("ENTER CS msg received from: " + msg.getNickname());
			isInCriticalSection = true;
			actorInCS = Optional.of(msg.getNickname());
			log(actorInCS.get() + "entered in CS.");
			for (ParticipantInfo p: participants.values()) { 
				p.getWho().tell(new UserEnteredInCSMsg(msg.getNickname()),getSelf()); 
			}
			getTimers().startSingleTimer(SOLILOQUIUM_TIMER, new SoliloquiumTimeoutEvent() , Duration.ofMillis(SOLILOQUIUM_TIMEOUT));
			getContext().become(soliloquiumBehaviour);
		}).build();

		soliloquiumBehaviour = receiveBuilder().match(AddParticipantMsg.class, this::addParticipant)
		.match(RemoveParticipantMsg.class, this::removeParticipant)
		.match(DispatchChatMsg.class, msg -> {
			if (msg.getSenderNickname().equals(actorInCS.get())) {
				lastMsgTimestamp = System.currentTimeMillis();
				log("Chat msg received from " + msg.getSenderNickname() +" at "+lastMsgTimestamp+": "  + msg.getMsg());
				dispatchMsg(new ChatMsg(msg.getSenderNickname(), msg.getMsg()));
			} else {
				log("Discard Chat msg received from: " + msg.getSenderNickname() + "\n" + msg.getMsg());				
			}
		}).match(ExitCSMsg.class, msg -> {
			log("EXIT CS msg received from: " + msg.getNickname());
			if (this.actorInCS.get().equals(msg.getNickname())) {
				getTimers().cancel(SOLILOQUIUM_TIMER);		
				for (ParticipantInfo p: participants.values()) { 
					p.getWho().tell(new UserExitCSMsg(msg.getNickname()),getSelf()); 
				}
				checkForPendingSoliloquia();
			}
		}).match(SoliloquiumTimeoutEvent.class, msg -> {
			if (System.currentTimeMillis() - lastMsgTimestamp > SOLILOQUIUM_MAX_SILENCE_PERIOD) {
				log("TIMEOUT for soliloquium - forcing exit. ");
				for (ParticipantInfo p: participants.values()) { 
					p.getWho().tell(new UserForcedExitCSMsg(actorInCS.get()),getSelf()); 
				}
				checkForPendingSoliloquia();
			} else {
				getTimers().startSingleTimer(SOLILOQUIUM_TIMER, new SoliloquiumTimeoutEvent() , Duration.ofMillis(SOLILOQUIUM_TIMEOUT));
			}
		}).match(EnterCSMsg.class, msg -> {
			if (!actorInCS.get().equals(msg.getNickname()) && !soliloqPendingRequests.contains(msg.getNickname())){
				log("NEW SOLILOQIUM request.");
				soliloqPendingRequests.add(msg.getNickname());
			}
		}).build();		
		
		return normalModeBehaviour;
	}

	private void addParticipant(AddParticipantMsg msg) {
		log("New participant: " + msg.getNickname());
		participants.put(msg.getNickname(), new ParticipantInfo(msg.getWho(), msg.getNickname()));		
		for (ParticipantInfo p : participants.values()) {
			p.getWho().tell(new NewChatUserMsg(msg.getNickname()), getSelf());
		}
	}
	
	private void removeParticipant(RemoveParticipantMsg msg) {
		log("Remove participant: " + msg.getNickname());
		participants.remove(msg.getNickname());
		for (ParticipantInfo p : participants.values()) {
			p.getWho().tell(new QuitMsg(msg.getNickname()), getSelf());
		}
	}
	
	private void checkForPendingSoliloquia() {
		if (soliloqPendingRequests.isEmpty()) {
			restoreNormalMode();
		} else {
			String user = soliloqPendingRequests.removeFirst();
			if (participants.containsKey(user)) {
				actorInCS = Optional.of(user);
				log(actorInCS + "entered in CS.");
				for (ParticipantInfo p: participants.values()) { 
					p.getWho().tell(new UserEnteredInCSMsg(user),getSelf()); 
				}
				getTimers().startSingleTimer(SOLILOQUIUM_TIMER, new SoliloquiumTimeoutEvent() , Duration.ofMillis(SOLILOQUIUM_TIMEOUT));
			} else {
				restoreNormalMode();
			}
		}
	}
	
	private void dispatchMsg(ChatMsg msg) {
		for (ParticipantInfo p : participants.values()) {
			p.getWho().tell(msg, getSelf());
		}
	}
	
	private void restoreNormalMode() {
		isInCriticalSection = false;
		actorInCS = Optional.empty();
		getContext().become(normalModeBehaviour);
	}
	
	private void log(String msg) {
		synchronized (System.out) {
			System.out.println("[ broker ] " + msg);
		}
	}
}
