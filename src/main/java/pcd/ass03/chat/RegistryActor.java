package pcd.ass03.chat;

import java.time.Duration;
import java.util.HashMap;

import akka.actor.*;
import pcd.ass03.chat.msgs.AddParticipantMsg;
import pcd.ass03.chat.msgs.ChatMsg;
import pcd.ass03.chat.msgs.ChatUserKeepAliveMsg;
import pcd.ass03.chat.msgs.ChatUserKeepAliveReplyMsg;
import pcd.ass03.chat.msgs.InputMsg;
import pcd.ass03.chat.msgs.JoinMsg;
import pcd.ass03.chat.msgs.JoinedMsg;
import pcd.ass03.chat.msgs.NicknameAlreadyUsed;
import pcd.ass03.chat.msgs.QuitMsg;
import pcd.ass03.chat.msgs.RemoveParticipantMsg;

public class RegistryActor extends AbstractActorWithTimers {

	private HashMap<String,ParticipantInfo> participants;
	private ActorSelection broker;
	private String brokerPath;
	private long lastKeepalive;
    
	private final String KEEP_ALIVE_TIMER = "keepAliveTimer";
	private final long KEEP_ALIVE_PERIOD = 5000;
	private final String KEEP_ALIVE_TIMEOUT_TIMER = "keepAliveTimerTimout";
	private final long KEEP_ALIVE_TIMEOUT_PERIOD = 2000;
	
	public RegistryActor(String brokerPath) {
		  participants = new HashMap<String,ParticipantInfo>();
		  this.brokerPath = brokerPath;
		  this.broker = getContext().actorSelection(brokerPath);
	}
	
	public void preStart() {
		getTimers().startPeriodicTimer(KEEP_ALIVE_TIMER, new TimeToKeepAliveEvent(), Duration.ofMillis(KEEP_ALIVE_PERIOD));
		log("ready.");
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
		.match(JoinMsg.class, msg -> {
			  log("Join requested: "+  msg.getNickname());
			  ParticipantInfo p = participants.get(msg.getNickname());
			  if (p != null) {
				  log("Not accepted - already there. ");
				  getSender().tell(new NicknameAlreadyUsed(), getSelf());
			  } else {
				  log("Accepted.");
				  participants.put(msg.getNickname(), new ParticipantInfo(msg.getWho(), msg.getNickname()));
				  broker.tell(new AddParticipantMsg(msg.getWho(), msg.getNickname()), getSelf());
				  getSender().tell(new JoinedMsg(brokerPath), getSelf());				  
			  }
		})
		.match(TimeToKeepAliveEvent.class, msg -> {
			// log("Keep alive timeout: checking who is alive at "+System.currentTimeMillis());
			ChatUserKeepAliveMsg keepAliveMsg = new ChatUserKeepAliveMsg();
			for (ParticipantInfo p: participants.values())  {
				p.getWho().tell(keepAliveMsg, getSelf());
			}
			lastKeepalive = System.currentTimeMillis();
			getTimers().startSingleTimer(KEEP_ALIVE_TIMEOUT_TIMER, new KeepAliveTimeoutEvent(), Duration.ofMillis(KEEP_ALIVE_TIMEOUT_PERIOD));
		})
		.match(KeepAliveTimeoutEvent.class, msg -> {
			for (ParticipantInfo p: participants.values())  {
				if (p.getLastKeepaliveTimestamp() < lastKeepalive) {
					participants.remove(p.getNickname());
					log("Removed "+p.getNickname());
					broker.tell(new RemoveParticipantMsg(p.getNickname()), getSelf());			  
				}
			}
		})
		.match(ChatUserKeepAliveReplyMsg.class, msg -> {
			// log("Keep alive Msg from user "+msg.getNickname());			
			ParticipantInfo p = participants.get(msg.getNickname());
			if (p != null) {
				p.updateKeepaliveTimestamp(System.currentTimeMillis());
			}
		})
		.match(QuitMsg.class, msg -> {
			  log("Quit requested: "+  msg.getNickname());
			  ParticipantInfo p = participants.remove(msg.getNickname());
			  if (p != null) {
				  broker.tell(new RemoveParticipantMsg(msg.getNickname()), getSelf());			  
			  } 
		})
		.build();
	}

	private void log(String msg) {
		synchronized (System.out) {
			System.out.println("[ registry ] "+msg);
		}
	}
	
}
