package pcd.ass03.chat;

import java.io.File;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class ChatServerMain {
	public static void main(String[] args) {

		String serverPort;
		if (args.length == 0) {
			serverPort = "2552";			
		} else {
			serverPort = args[0];
		}

		String systemName = "Chat";
		String brokerHost = "127.0.0.1";
		
		System.out.println(".:: CHAT SYSTEM - port: "+serverPort+" ::.");

		Config conf = ConfigFactory.parseString(makeConfig(serverPort));
		ActorSystem system = ActorSystem.create(systemName, conf);

		system.actorOf(Props.create(BrokerActor.class), "broker");

		String brokerPath = "akka.tcp://"+systemName+"@"+brokerHost+":" + serverPort + "/user/broker";
		system.actorOf(Props.create(RegistryActor.class, brokerPath), "registry");
	}
	
	static private String makeConfig(String serverPort) {
		return 
				"akka {\n" + 
				"  actor {\n" + "    provider = remote\n" + "  }\n" + 
				"  remote {\n"+ 
				"    enabled-transports = [\"akka.remote.netty.tcp\"]\n" + 
				"    netty.tcp {\n"+ 
				"      hostname = \"127.0.0.1\"\n" + 
				"      port = " + serverPort + "\n" + 
				"    }\n" + 
				" }\n" + 
				"}";
	}

}
